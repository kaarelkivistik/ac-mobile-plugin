import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import styled, { ThemeProvider } from "styled-components";
import DocumentProvider, { useDocument } from "./DocumentProvider";
import Footer from "./Footer";
import Header from "./Header";
import Today from "./Today";
import ViewThread from "./ViewThread";
import themes from "./themes.json";

const App = () => {
  React.useEffect(() => {
    const responsiveMeta = document.createElement("meta");
    responsiveMeta.name = "viewport";
    responsiveMeta.content = "width=device-width";
    document.head.appendChild(responsiveMeta);

    return () => {
      document.head.removeChild(responsiveMeta);
    };
  }, []);

  return (
    <BrowserRouter>
      <DocumentProvider>
        <ACThemeProvider>
          <Container>
            <Header />
            <Body>
              <Switch>
                <Route path="/today.php" component={Today} />
                <Route path="/viewthread.php" component={ViewThread} />
              </Switch>
            </Body>
            <Footer />
          </Container>
        </ACThemeProvider>
      </DocumentProvider>
    </BrowserRouter>
  );
};

const ACThemeProvider = props => {
  const $document = useDocument();
  const logoImage = $document.querySelector('img[usemap="#ac"]');

  const isHele = logoImage.src.endsWith("/images/hele_theme/hele_logo.gif");
  const isTume = logoImage.src.endsWith("/images/tume_theme/tume_logo.gif");

  const theme = isHele ? themes.hele : isTume ? themes.tume : themes.default;

  return <ThemeProvider theme={theme} {...props} />;
};

const Body = styled.div`
  font-family: ${props => props.theme.font};
  font-size: ${props => props.theme.fontsize};

  padding: 1.5rem 0;

  div {
    border-color: ${props => props.theme.bordercolor};
  }

  * {
    box-sizing: border-box;
  }
`;

const Container = styled.div`
  @media (min-width: 600px) {
    padding: 0 1.7rem;
  }

  > table,
  ${Body} > table {
    width: 100%;
  }
`;

export default App;
