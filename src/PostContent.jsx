import { domToReact } from "html-react-parser";
import React from "react";
import styled from "styled-components";
import Content, { contentParserOptions } from "./Content";

const PostContent = (props) => <Content {...props} parserOptions={parserOptions()} />;

const parserOptions = () => ({
  replace: (node) => {
    const { name, attribs = {}, children } = node;
    const { src, class: className = "" } = attribs;

    if (name === "img" && !src.startsWith(`./images/smilies`)) return <Image src={attribs.src} />;
    else if (name === "table" && className.split(" ").includes("quote")) return <Quote>{node}</Quote>;
    else if (name === "a")
      return (
        <a href={attribs.href} target="_blank" rel="noopener nofollow">
          {domToReact(children, parserOptions())}
        </a>
      );

    return contentParserOptions(parserOptions).replace(node);
  },
});

const Image = styled.img`
  max-width: 100%;
  height: auto;
`;

const Quote = ({ children: node }) => (
  <QuoteContainer>
    <div>Tsitaat:</div>
    <div>
      <PostContent>{node.children[0].children[1].children[0].children}</PostContent>
    </div>
  </QuoteContainer>
);

const QuoteContainer = styled.div`
  border: 1px solid ${(props) => props.theme.bordercolor};
  margin: 1rem;

  > div {
    padding: 0.5rem;
    background: ${(props) => props.theme.altbg2};
  }

  > div:first-child {
    border-bottom: 1px solid ${(props) => props.theme.bordercolor};
    font-weight: bold;
    background: ${(props) => props.theme.header};
  }
`;

export default PostContent;
