import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

const hasMatchingRoute = () => window.location.pathname.substr(1).match(/^(today|viewthread)\.php/);

const hideEverything = () => {
  const style = document.createElement("style");
  style.innerHTML = `
    body > *:not(#root) {
      display: none;
    }
  `;
  document.head.appendChild(style);
};

if (hasMatchingRoute()) hideEverything();

document.addEventListener("DOMContentLoaded", () => {
  if (!hasMatchingRoute()) return;

  const root = document.createElement("div");
  root.id = "root";
  document.body.appendChild(root);

  ReactDOM.render(<App />, root);
});
