/* today.php */

export const getThreads = document =>
  Array.from(document.querySelectorAll("tr.tablerow"))
    .filter(row => [...row.childNodes].filter(node => node.tagName === "TD").length === 7)
    .map(threadFromNode);

const threadFromNode = row => {
  const iconMatch = row.querySelector("img");
  const lastPageMatch = row.querySelector("a ~ small a:last-of-type");

  return {
    icon: iconMatch && iconMatch.src,
    url: removeOrigin(row.querySelector("a").href),
    title: row.querySelector("a").innerText,
    pages: lastPageMatch ? parseInt(lastPageMatch.innerText) : 1,
    author: profileFromNode(row.querySelector("td:nth-child(3) a")),
    forum: forumFromNode(row.querySelector("td:nth-child(4) a")),
    replies: parseInt(row.querySelector("td:nth-child(5)").innerText),
    views: parseInt(row.querySelector("td:nth-child(6)").innerText),
    lastPost: lastPostFromNode(row.querySelector("td:nth-child(7)"))
  };
};

const lastPostFromNode = td => ({
  dateTime: td.querySelector("font").firstChild.textContent.trim(),
  author: profileFromNode(td.querySelector("font").querySelector("a")),
  url: removeOrigin(td.querySelector("td:nth-child(2) a").href)
});

const profileFromNode = a => ({
  url: removeOrigin(a.href),
  name: a.innerText
});

const forumFromNode = a => ({
  url: removeOrigin(a.href),
  name: a.innerText
});

const removeOrigin = value => value.replace(location.origin, "");

/* viewthread.php */

export const getThreadMeta = document => {
  const headerRow = [...document.querySelectorAll("table:nth-of-type(4) tr tr")].find(
    row => !row.innerText.trim().startsWith("Lehekülgi:")
  );
  const statsCell = headerRow.querySelector("td[align=right]");
  const [views, replies] = statsCell.innerText
    .trim()
    .match(/Vastuseid: ([0-9]+).*Vaatamisi: ([0-9]+)/)
    .slice(1)
    .map(value => parseInt(value));

  const pagesMatch = document.querySelector('td.multi[colspan="2"] u:last-child, td.multi[colspan="2"] a:last-child');

  return {
    title: headerRow
      .querySelector("td[align=left]")
      .innerText.trim()
      .match(/^Pealkiri: (.+)/)[1],
    views,
    replies,
    pages: (pagesMatch && parseInt(pagesMatch.innerText)) || 1
  };
};

export const getPosts = document =>
  Array.from(document.querySelectorAll("body > table:nth-of-type(4) > tbody > tr > td > table > tbody > tr"))
    .filter(row => !row.innerText.trim().startsWith("Autor:") && !row.innerText.trim().startsWith("Lehekülgi:"))
    .map(postFromNode);

const postFromNode = row => ({
  pid: row.querySelector("a[name^=pid]").name,
  timestamp: row
    .querySelector("td:nth-child(2) td td")
    .innerText.trim()
    .match(/^postitati (.+)$/m)[1],
  actions: row.querySelector("td:nth-child(2) td td:nth-child(2)"),
  profileActions: row.querySelector("td:nth-of-type(2) > table > tbody > tr:last-child td.smalltxt").innerHTML,
  subject: row.querySelector("tr.messagebody > td > font.subject"),
  content: row.querySelector("tr.messagebody > td > font.mediumtxt").innerHTML,
  author: authorFromNode(row.querySelector("tr > td.tablerow")),
  signature: row.querySelector("div.sig")
});

const authorFromNode = td => {
  const avatarImage = td.querySelector("div[align=center]:nth-of-type(2) > img");

  const metaNode = td.querySelector("div.smalltxt");

  const meta = metaNode.innerText.trim();
  const locationMatch = meta.match(/^Asukoht: (.+)/m);
  const carMatch = meta.match(/^Auto: (.+)/m);

  return {
    name: td.querySelector("font.mediumtxt").innerText.trim(),
    status: meta.match(/^.*/)[0].trim(),
    signedUpDate: meta.match(/Registreerunud ([0-9\.]+)/)[1],
    location: locationMatch && locationMatch[1],
    isOnline: !!meta.match(/^Kasutaja on foorumis$/m),
    car: carMatch && carMatch[1],
    avatar: avatarImage && avatarImage.src
  };
};
