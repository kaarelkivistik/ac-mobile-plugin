const express = require("express");
const proxy = require("http-proxy-middleware");
const iconv = require("iconv-lite");
const cheerio = require("cheerio");

const { PORT = 8080 } = process.env;

const app = express();

app.use(express.static("dist"));

app.use(
  proxy((path) => path === "/" || path.endsWith(".php"), {
    target: "https://foorum.audiclub.ee",
    headers: {
      "accept-encoding": "identity",
    },
    changeOrigin: true,
    cookieDomainRewrite: "",
    onProxyRes: (proxyRes, req, res) => {
      const writeResponse = res.write;
      res.write = () => {};

      let body = "";
      proxyRes.on("data", (data) => {
        body += iconv.decode(data, "windows1252");
      });

      proxyRes.on("end", () => writeResponse.call(res, iconv.encode(addScripts(body), "windows1252")));
    },
  })
);

const addScripts = (body) => {
  const $ = cheerio.load(body);

  $("head").append(`<script src="/bundle.js"></script>`);

  return $.xml();
};

app.use(proxy({ target: "https://foorum.audiclub.ee", changeOrigin: true }));

app.listen(PORT);

process.on("SIGINT", () => process.exit());
process.on("SIGTERM", () => process.exit());
