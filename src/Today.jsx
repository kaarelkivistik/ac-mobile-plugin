import React from "react";
import MediaQuery from "react-responsive";
import { Link } from "react-router-dom";
import styled, { ThemeConsumer } from "styled-components";
import { getThreads } from "./api";
import { Pages } from "./components";
import { useDocument } from "./DocumentProvider";

const Today = () => {
  const $document = useDocument();
  const threads = $document ? getThreads($document) : [];

  return <Threads threads={threads} />;
};

const Threads = ({ threads }) => (
  <ThreadsContainer>
    <MediaQuery maxWidth={599}>
      {threads.map((thread, index) => (
        <MobileThread key={index} thread={thread} />
      ))}
    </MediaQuery>
    <MediaQuery minWidth={600}>
      <Header />
      {threads.map((thread, index) => (
        <Thread key={index} thread={thread} />
      ))}
    </MediaQuery>
  </ThreadsContainer>
);

const Header = () => (
  <HeaderRow>
    <Column centered />
    <Column>Pealkiri:</Column>
    <Column centered>Autor:</Column>
    <Column centered>Foorum:</Column>
    <Column centered>Vastuseid:</Column>
    <Column centered>Vaatamisi:</Column>
    <Column>Viimane postitus:</Column>
  </HeaderRow>
);

const MobileThread = ({ thread }) => (
  <MobileThreadContainer to={thread.lastPost.url}>
    <img src={thread.icon} />
    <span>
      {thread.title} {thread.pages > 1 && <small>(Lehekülgi:&nbsp;{thread.pages})</small>}
      <br />
      <small>
        {thread.lastPost.author.name} » {thread.lastPost.dateTime} » {thread.forum.name}
      </small>
    </span>
  </MobileThreadContainer>
);

const MobileThreadContainer = styled(Link)`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0.6rem;

  border: 1px solid ${props => props.theme.bordercolor};

  &:not(:last-child) {
    border-bottom: none;
  }

  img {
    margin-right: 1rem;
  }

  &:nth-child(odd) {
    background: ${props => props.theme.altbg1};
  }

  &:nth-child(even) {
    background: ${props => props.theme.altbg2};
  }
`;

const ThreadsContainer = styled.div`
  margin: 1rem 0;
`;

const Thread = ({ thread }) => (
  <ThemeConsumer>
    {theme => (
      <ThreadContainer>
        <Column centered>
          <img src={thread.icon} />
        </Column>
        <Column>
          <Link to={thread.url}>{thread.title}</Link>&nbsp;
          {thread.pages > 1 && (
            <small>
              (<Pages url={thread.url} pages={thread.pages} />)
            </small>
          )}
        </Column>
        <Column centered>{thread.author.name}</Column>
        <Column centered>{thread.forum.name}</Column>
        <Column centered>{thread.replies}</Column>
        <Column centered>{thread.views}</Column>
        <Column right>
          <LastPostAuthor>
            <div>{thread.lastPost.dateTime}</div>
            <div>Postitaja: {thread.lastPost.author.name}</div>
          </LastPostAuthor>
          <Link to={thread.lastPost.url}>
            <img src={`${theme.imgdir}/lastpost.gif`} />
          </Link>
        </Column>
      </ThreadContainer>
    )}
  </ThemeConsumer>
);

const LastPostAuthor = styled.div`
  text-align: right;
  margin-right: 2rem;
`;

const Column = styled.div`
  flex: 1 1 ${props => props.base};
  padding: 4px;
  text-overflow: ellipsis;
  overflow: hidden;
  display: flex;
  flex-wrap: wrap;
  align-items: center;

  text-align: ${props => props.centered && "center"};
  justify-content: ${props => (props.centered ? "center" : props.right && "flex-end")};
`;

const Row = styled.div`
  display: flex;

  ${Column}:nth-child(1) {
    flex: 1 1 4%;
  }
  ${Column}:nth-child(2) {
    flex: 1 1 43%;
  }
  ${Column}:nth-child(3) {
    flex: 1 1 14%;
  }
  ${Column}:nth-child(4) {
    flex: 1 1 14%;
  }
  ${Column}:nth-child(5) {
    flex: 1 1 5%;
  }
  ${Column}:nth-child(6) {
    flex: 1 1 5%;
  }
  ${Column}:nth-child(7) {
    flex: 1 1 15%;
  }

  ${Column} {
    border: 1px solid ${props => props.theme.bordercolor};

    &:not(:last-child) {
      border-right: none;
    }
  }

  &:not(:last-child) {
    ${Column} {
      border-bottom: none;
    }
  }
`;

const HeaderRow = styled(Row)`
  background: ${props => props.theme.header};
  font-weight: bold;
  color: ${props => props.theme.headertext};
`;

const ThreadContainer = styled(Row)`
  ${Column}:nth-child(odd) {
    background: ${props => props.theme.altbg1};
  }

  ${Column}:nth-child(even) {
    background: ${props => props.theme.altbg2};
  }
`;

export default Today;
