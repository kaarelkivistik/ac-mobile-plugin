import React from "react";
import Content from "./Content";
import { useDocument } from "./DocumentProvider";

const Footer = () => {
  const $document = useDocument();

  return (
    <>
      <Content>{$document.querySelector("body > table:nth-last-of-type(2)")}</Content>
      <Content>{$document.querySelector("body > table:last-of-type")}</Content>
    </>
  );
};

export default Footer;
