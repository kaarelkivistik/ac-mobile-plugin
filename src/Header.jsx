import { domToReact } from "html-react-parser";
import React from "react";
import styled from "styled-components";
import Content, { contentParserOptions } from "./Content";
import { useDocument } from "./DocumentProvider";

const Header = () => {
  const $document = useDocument();

  return (
    <HeaderContainer>
      <Content parserOptions={parserOptions()}>{$document.querySelector("table")}</Content>
      <Content>{$document.querySelector("table:nth-of-type(2)")}</Content>
    </HeaderContainer>
  );
};

const HeaderContainer = styled.div`
  & > table {
    width: 100%;
  }

  @media (max-width: 992px) {
    td[align="center"][valign="top"] {
      display: none;
    }
  }
`;

const parserOptions = () => ({
  replace: (node) => {
    const { name, children, attribs: { style = "", onclick = "", ...attribs } = {} } = node;

    if (onclick.startsWith("Popup('u2u.php'")) {
      const colorMatch = style.match(/;?color:(.+);?/);
      const color = colorMatch && colorMatch[1];

      return (
        <a href="#" onClick={() => Popup("u2u.php", "Window", 700, 450)} style={{ color }}>
          {domToReact(children)}
        </a>
      );
    } else if (name === "area" && attribs.title === "Foorum") {
      return <area shape="rect" coords="9,16,67,24" href="/" title="Foorumz" alt="Foorum" />;
    } else if (name === "img" && attribs.usemap === "#ac") {
      return <img src={attribs.src} alt={attribs.alt} border={attribs.border} usemap="#ac-mobile"></img>;
    } else if (name === "map" && attribs.name === "ac") {
      return <map name="ac-mobile">{domToReact(children, parserOptions())}</map>;
    }

    return contentParserOptions().replace(node);
  },
});

export default Header;
