export const parseQueryString = search =>
  search
    .substr(1)
    .split("&")
    .map(pair => pair.split("=").map(token => decodeURIComponent(token)))
    .reduce((query, [key, value]) => {
      query[key] = value;
      return query;
    }, {});
