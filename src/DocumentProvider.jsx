import React, { useEffect } from "react";
import { useQuery } from "react-query";
import { withRouter } from "react-router";

export const DocumentContext = React.createContext();

const DocumentProvider = withRouter(({ location, children }) => {
  const isFirstNavigation = React.useRef(true);
  const url = location.pathname + location.search;

  return (
    <CurrentDocumentProvider key={url} url={url} isFirstNavigation={isFirstNavigation}>
      {children}
    </CurrentDocumentProvider>
  );
});

const CurrentDocumentProvider = ({ url, isFirstNavigation, children }) => {
  const { data: $document } = useQuery(["document", url], async () =>
    isFirstNavigation.current
      ? document
      : fetch(url)
          .then((response) => response.text())
          .then((text) => new DOMParser().parseFromString(text, "text/html"))
  );

  useEffect(() => {
    isFirstNavigation.current = false;
  }, []);

  if (!$document) return null;

  return <DocumentContext.Provider value={$document}>{children}</DocumentContext.Provider>;
};

export const useDocument = () => {
  return React.useContext(DocumentContext);
};

export default DocumentProvider;
