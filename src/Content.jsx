import parse, { domToReact } from "html-react-parser";
import React from "react";
import { Link } from "react-router-dom";

const Content = ({ children, parserOptions = contentParserOptions() }) => {
  if (!children) return null;
  if (children instanceof Node) return parse(children.outerHTML, parserOptions);
  if (typeof children === "string") return parse(children, parserOptions);

  return domToReact(children, parserOptions);
};

export const contentParserOptions = (getInnerOptions = contentParserOptions) => ({
  replace: (node) => {
    const { type, name, parent, attribs = {}, data = "" } = node;
    const { href = "" } = attribs;

    if (type === "text" && parent && ["table", "tbody", "tr"].includes(parent.name) && !data.trim())
      return <React.Fragment />;

    if (name === "a")
      return href.match(/^(today|viewthread)\.php/) ? (
        <Link to={href}>{domToReact(node.children, getInnerOptions())}</Link>
      ) : (
        <a href={href}>
          {domToReact(node.children, getInnerOptions())}
        </a>
      );
  },
});

export default Content;
