import React from "react";
import { NavLink } from "react-router-dom";

export const Pages = ({ url, pages }) => (
  <>
    Lehekülgi <PageLink url={url} page={1} /> <PageLink url={url} page={2} />
    {pages > 2 && (
      <>
        ... <PageLink url={url} page={pages} />
      </>
    )}
  </>
);

const PageLink = ({ url, page, ...props }) => (
  <NavLink to={`${url}&page=${page}`} {...props}>
    {page}
  </NavLink>
);
