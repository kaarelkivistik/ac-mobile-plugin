import React, { useEffect } from "react";
import { useHistory, useLocation } from "react-router";
import styled, { css } from "styled-components";
import { getPosts, getThreadMeta } from "./api";
import Content from "./Content";
import { useDocument } from "./DocumentProvider";
import PostContent from "./PostContent";
import { parseQueryString } from "./shared";

const ViewThread = () => {
  const $document = useDocument();
  const pagesMatch = $document.querySelector(`td.multi[colspan="2"]`);
  const { title } = getThreadMeta($document);

  return (
    <>
      <Content>{$document.querySelector("table:nth-of-type(3)")}</Content>
      {pagesMatch && (
        <PagesRow>
          <Content>{pagesMatch.innerHTML}</Content>
        </PagesRow>
      )}
      <HeaderRow>
        <div>Autor:</div>
        <div>Pealkiri: {title}</div>
      </HeaderRow>
      <Posts />
      {pagesMatch && (
        <PagesRow>
          <Content>{pagesMatch.innerHTML}</Content>
        </PagesRow>
      )}
    </>
  );
};

const PagesRow = styled.div`
  background: ${(props) => props.theme.altbg1};
  padding: 0.25rem;
  border: 1px solid ${(props) => props.theme.bordercolor};

  &:first-of-type {
    border-bottom: none;
  }
`;

const Posts = () => {
  const $document = useDocument();
  const posts = getPosts($document);

  const history = useHistory();
  const location = useLocation();

  const { goto } = parseQueryString(location.search);
  const lastPostPid = posts.length > 0 && goto === "lastpost" ? posts[posts.length - 1].pid : null;
  const focusedPid = location.hash.substr(1);

  useEffect(() => {
    if (lastPostPid && !focusedPid) {
      document.querySelector(`a[name=${lastPostPid}]`).scrollIntoView();
      history.replace(location.pathname + location.search + `#${lastPostPid}`);
    }
  }, [focusedPid, focusedPid]);

  return posts.map((post) => <Post key={post.pid} post={post} />);
};

const Post = ({ post }) => (
  <Row>
    <a name={post.pid} />
    <Profile user={post.author} />
    <Body>
      <Header>
        <span>postitati {post.timestamp}</span>
        {post.actions && (
          <Actions>
            <Content>{post.actions.innerHTML}</Content>
          </Actions>
        )}
      </Header>
      <ContentContainer>
        <PostContent>{post.content}</PostContent>
      </ContentContainer>
      {post.signature && <Signature dangerouslySetInnerHTML={{ __html: post.signature.innerHTML }} />}
      <Footer>
        <Actions>
          <Content>{post.profileActions}</Content>
        </Actions>
      </Footer>
    </Body>
  </Row>
);

const Body = styled.div`
  flex: 1 1 82%;
  display: flex;
  flex-direction: column;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Signature = styled.div``;

const Profile = ({ user }) => (
  <AuthorContainer>
    <p>
      <strong>{user.name}</strong>
      <br />
      {user.isClubMember ? <i>{user.status}</i> : user.status}
    </p>

    {user.avatar && <Avatar src={user.avatar} />}

    <Meta>
      Registreerunud {user.signedUpDate}
      <br />
      {user.location && (
        <>
          Asukoht: {user.location}
          <br />
        </>
      )}
      {user.isOnline ? <strong>Kasutaja on foorumis</strong> : <span>Kasutaja on eemal</span>}
    </Meta>

    {user.car && (
      <Meta>
        <strong>Auto:</strong> {user.car}
      </Meta>
    )}
  </AuthorContainer>
);

const Meta = styled.p`
  width: 100%;
`;

const Avatar = styled.img``;

const AuthorContainer = styled.div`
  flex: 1 1 18%;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;

  ${Avatar} {
    align-self: center;
    max-width: 100%;
  }

  @media (max-width: 599px) {
    flex-direction: row;
    align-items: center;

    ${Avatar} {
      order: -1;
      height: 2rem;
      margin-right: 1rem;
    }

    ${Meta} {
      display: none;
    }
  }

  @media (min-width: 600px) {
    p:first-child {
      margin-top: 0;
    }
  }
`;

const ContentContainer = styled.div`
  word-break: break-word;
`;

const Actions = styled.div`
  margin: 0 -0.1rem;

  > * {
    margin: 0 0.1rem;
  }
`;

const Footer = styled.div``;

const borderColor = css`
  ${(props) => props.theme.bordercolor}
`;

const borders = css`
  border: 1px solid ${borderColor};

  &:not(:last-child) {
    border-bottom: none;
  }

  ${AuthorContainer}, ${Header} {
    border-bottom: 1px solid ${borderColor};
  }

  ${Signature} {
    border-top: 1px dashed ${borderColor};
  }

  ${Footer} {
    border-top: 1px solid ${borderColor};
  }

  @media (min-width: 600px) {
    ${AuthorContainer} {
      border-right: 1px solid ${borderColor};
      border-bottom: none;
    }
  }
`;

const colors = css`
  &:nth-child(odd) {
    background-color: ${(props) => props.theme.altbg1};
  }

  &:nth-child(even) {
    background-color: ${(props) => props.theme.altbg2};
  }
`;

const spacing = css`
  ${AuthorContainer}, ${Header}, ${Signature}, ${Footer} {
    padding: 0.5rem;
  }

  ${ContentContainer} {
    padding: 1.5rem 0.5rem;
    flex-grow: 1;
  }
`;

const Row = styled.div`
  display: flex;
  align-items: stretch;
  flex-direction: column;

  ${borders}
  ${colors}
  ${spacing}

  @media (min-width: 600px) {
    flex-direction: row;
  }
`;

const HeaderRow = styled.div`
  display: flex;
  background: ${(props) => props.theme.header};
  font-weight: bold;
  color: ${(props) => props.theme.headertext};

  border: 1px solid;
  border-bottom: none;

  & > div:first-child {
    flex: 0 0 18%;
    border-right: 1px solid ${(props) => props.theme.bordercolor};

    @media (max-width: 599px) {
      display: none;
    }
  }

  & > div {
    flex: 0 0 auto;
    padding: 0.3rem 0.5rem;
    width: 100%;
  }
`;

export default ViewThread;
